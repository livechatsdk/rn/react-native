import { Alert, PermissionsAndroid, Platform } from 'react-native';

//var lvsock = require( './../lib/index' );

import AVChatConnection from './LivechatAv'
//import sdkHandler from '../lib/index';

function logi(msg){
    global.log.func("info", "CallSession: " + msg);
    //console.log( "Info CallSession: " + msg );
    return msg;
}

function logw(msg){
    global.log.func("warn", "CallSession: " + msg);
    //console.log( "Warn CallSession: " + msg );
    return msg;
}

function loge(msg){
    global.log.func("error", "CallSession: " + msg);
    //console.log( "Error CallSession: " + msg );
    return msg;
}

function logd(msg){
    global.log.func( "debug", "CallSession: " + msg);
    //console.log( "Debug CallSession: " + msg );
    return msg;
}

export default class CallSesssion
{
    currentState;
    config;
    mode;
    callid;
    userid;
    remoteUserId;
    remoteUserName;
    useridResolver;
    livechatapi;
    formatedCallTime;
    callstartTime;
    isMute; isLoud; hasVideo;
    localIceDetected; remoteIceReceived;
    remoteStreamMap;

    constructor(sessionConfig)
    {
        this.remoteStreamMap = {};

        this.localIceDetected = this.remoteIceReceived = false;

        this.currentState = this.StateEvent.UNCONNECTED;
        this.config = sessionConfig;
        this.avconn = new AVChatConnection();
        this.avconn.enableLogEvents(true);
        if( global.configs.debug )
        {
            console.log( "debug mode" );
            //lvsock = require( './comms/SimpleWs' );
        }

        console.log( "CallSession created" );
    }

    release = () =>
    {
        this.avconn.release();
    }

    extToInt = {
        video: "video",
        audio: "audio",
        screenshare: "screen",
        screen: "screen"    // won't be used
    };

    intToExt = {
        video: "video",
        audio: "audio",
        screenshare: "screenshare", // won't be used
        screen: "screenshare"
    };

    StateEvent = { // indexes 0 - 20
        UNCONNECTED :   { name:"UNCONNECTED" , index:0, handlers:null, desc:"Unconnected", usertxt:"Unconnected" }, // no events, just the starting state

        // mode = ( 'audio' | 'video' ) , offering = ( 'audio' | 'video' | 'screen' ) ,  callid, resolvedRemoteName )
        MEDIA_REQ_RECEIVED : { name:"MEDIA_REQ_RECEIVED", index:1, handlers:{}, desc:"Remote party requested media", usertxt:"Incomming call .." }, 
        
        // mode = ( 'audio' | 'video' )
        MEDIA_REQ_SENT : { name:"MEDIA_REQ_SENT", index:2, handlers:{}, desc:"Calling a remote user", usertxt:"Calling .." },
        MEDIA_REQ_ACKED : { name:"MEDIA_REQ_ACKED", index:3, handlers:{}, desc:"Remote party is prompting for media", usertxt:"Ringing .." }, // ( 'audio' | 'video' )
        MEDIA_REQ_TIMEDOUT : { name:"MEDIA_REQ_TIMEDOUT", index:4, handlers:{}, desc:"End of ringing without a response from remote or local user" }, // no params
        
        CONNECTING_MEDIA : { name:"CONNECTING_MEDIA", index:5, handlers:{}, desc:"Trying to estabish media", usertxt:"Connecting for media .." }, // ( 'audio' | 'video' )
        
        RECONNECTING_MEDIA : { name:"RECONNECTING_MEDIA", index:6, handlers:{}, desc:"Trying to re-establish media", usertxt:"Reconnecting media .." }, // void
        MEDIA_RECONNECTABLE : { name:"MEDIA_RECONNECTABLE", index:7, handlers:{}, desc:"Media connectivity is down but user can call reconnect", usertxt:"retry please .." }, // void
        INCALL_MEDIA : { name:"INCALL_MEDIA", index:8, handlers:{}, desc:"Agent answered and in text + media", usertxt:"Media connected" }, // ( 'audio' | 'video' )
        REINCALL_MEDIA : { name:"REINCALL_MEDIA", index:9, handlers:{}, desc:"reconnected to media", usertxt:"Media reconnected" }, // ( 'audio' | 'video' )
        
        
        ENDED :        { name:"ENDED", index:10, handlers:{}, desc:"Call ended", usertxt:"Call ended"}, //( reason )
    };

    CommonEvent = { // indexes 20 - 50

        AVSTART_FAILED : { name:"AVSTART_FAILED",  index:20, handlers:{}, desc:"Audio or video call start failed" }, // ( reason )
        VIDEO_RECEIVED : { name:"VIDEO_RECEIVED", index:21, handlers:{}, desc:"Video received" }, // ( index, type )
        VIDEO_DROPPED : { name:"VIDEO_DROPPED", index:22, handlers:{}, desc:"Video dropped" },
        CALLSTART_FAILED : { name:"CALLSTART_FAILED", index:23, handlers:{}, desc:"Call negotiation failed" }, // ( errorCode )
        
        // mode = 'addscr', callid, user, resolvedRemoteName 
        SS_PERM_REQ_RECEIVED : { name:"SS_PERM_REQ_RECEIVED", index:25, handlers:{}, desc:"Permission request received to share screen from remote party" }, 
        
        // for future use 
        SS_PERM_REQ_SENT: { name:"SS_PERM_REQ_SENT", index:26, handlers:{}, desc:"Requested to add local screen to current call" },
        
        // errorCode
        SS_START_FAILED : { name :"SS_START_FAILED", index:27, handlers:{}, desc:"Screen negotiations failed/rejected" },
        
        // for future use, mode = 'requestscr' , callid, user, resolvedRemoteName
        SS_REQ_TIMEDOUT: { name:"SS_REQ_TIMEDOUT", index:28, handlers:{}, desc:"Screen share negotiation timed out" },


        SS_REQUEST_RECEIVED :{ name :"SS_REQUEST_RECEIVED", index:32, handlers:{}, desc:"Request received to share the screen" }, 
        SS_REQUEST_SENT :{ name :"SS_REQUEST_SENT", index:33, handlers:{}, desc:"Request sent asking to share remote screen" },

        CALL_TIME : { name:"CALL_TIME", index:38, handlers:{}, desc:"Call time" }, // ( fmtTimeStr, deltasec )
        CALL_STATE : { name:"CALL_STATE", index:39, handlers:{}, desc:"Call state changed" } // ( state, userid )
    };

    Errors = {

        CALL_NOT_ANSWERED : { name : "CALL_NOT_ANSWERED", desc:"Call was not answered by remote party" },
        CALL_CANCELED : { name : "CALL_CANCELED", desc:"", desc:"Call canceled before getting established" },
        LICENSE_VALIDATION_FAILED : { name : "LICENSE_VALIDATION_FAILED", desc:"Licensed critera violated" },
        CALEE_OFFLINE : { name : "CALEE_OFFLINE", desc:"Remote party being called is offline" },
        CALL_REJECTED : { name : "CALL_REJECTED", desc:"Remote party rejected the call while ringing" },
        SS_REJECTED : { name: "REJECTED", desc:"Screen share request rejected" },
        CALL_NOT_AVAILABLE :{ name: "CALL_NOT_AVAILABLE", desc:"There is no ongoing call perform requested operation" },
        CALL_INPROGRESS : { name: "DIFFERENT_CALL_INPROGRESS", desc:"Not possible to perform the request due to ongoing call" },

        ICE_NOT_FOUND : { name : "ICE_NOT_FOUND", desc : "Local ice candidates were not generated" },
        ICE_NOT_RECEIVED : { name : "ICE_NOT_RECEIVED", desc : "Didn't receive remote ice candidate" },
        ICE_NOT_CONNECTED : { name : "ICE_NOT_CONNECTED", desc:"Ice conenction wasn't established" },
        CALL_CANCELED_WHILE_CONNECTING : { name:"CALL_CANCELED_WHILE_CONNECTING", desc:"Call hangup by user while media was being established" },
        NORMAL_CALL_DISCONNECT : { name:"NORMAL_CALL_DISCONNECT", desc:"Normal disconnect by user" },
        CALL_DISCONNECTED_ABRUPTLY : { name:"CALL_DISCONNECTED_ABRUPTLY", desc:"internal disconnect" }

    };

    SetUserIdResolver = ( resolver ) =>
    {
        this.useridResolver = resolver;
    }

    IsInCallNego = () =>
    {
        return (
            (this.currentState.index > this.StateEvent.UNCONNECTED.index)
            && ( this.currentState.index < this.StateEvent.MEDIA_REQ_TIMEDOUT.index )
        );
    }

    IsActive = () =>
    {
        // Includes connecting state
        // effects ending and active call detection
        return (
            (this.currentState.index >= this.StateEvent.CONNECTING_MEDIA.index)
            && ( this.currentState.index < this.StateEvent.ENDED.index )
        );
    }

    IsRtcActive = () =>
    {
        // effects  callend and option retaining
        return (
            (this.currentState.index > this.StateEvent.CONNECTING_MEDIA.index)
            && ( this.currentState.index < this.StateEvent.ENDED.index )
        );
    }

    EndAvErrorForState = () =>
    {
        if( this.currentState == this.StateEvent.CONNECTING_MEDIA )
        {
            return this.Errors.CALL_CANCELED_WHILE_CONNECTING;
        }

        if( this.currentState.index >= this.StateEvent.MEDIA_REQ_SENT.index )
        {
           if( this.currentState.index < this.StateEvent.CONNECTING_MEDIA.index )
           {
                return this.Errors.CALL_CANCELED;
           }
        }

        return this.Errors.NORMAL_CALL_DISCONNECT;
    }

    isStateEvent=(event)=>{  return ( (event.index > -1) && ( event.index < 20 ) ); }

    SetConfig = (sessionConfig) => {

        this.config = sessionConfig;
    }

    invoke = ( event, ...evtargs ) => {

        if( this.isStateEvent( event ) )
        {
            if( this.currentState == event )
            {
                logd( "Already in " + event.name + " state, event canceled" );
                return;
            }
            this.currentState = event;
        }

        logi( "Dispatching event " + event.name + "( " + evtargs + " ) *" );

        for( regidProp in event.handlers )
        {
            if( event.handlers.hasOwnProperty( regidProp ) )
            {
                event.handlers[regidProp]( evtargs[0], evtargs[1], evtargs[2], evtargs[3], evtargs[4] );
            }
        }
    }

    AddHandler = ( event, regid, handler ) =>
    {
        if( event.handlers.hasOwnProperty( regid ) )
        {
            logw( "Handler (id=" + regid + ") replaced for event " + event.name );
        }

        event.handlers[regid] = handler;
    }

    RemoveHandler = ( stateEvent, regid ) =>
    {
        delete stateEvent.handlers[regid];
    }

    Initialize = ( userid ) =>
    {
        console.log( "CallSession Initialize" );

        this.userid = userid;
        this.currentState = this.StateEvent.UNCONNECTED;

        if( !this.config )
        {
            loge( "configs has not been set" );
            return;
        }

        this.livechatapi = window.livechat2Api;

        /*
        if( sdkHandler )
        {
            logi( "sdkHandler found" );
        }
        else
        {
            loge( "sdkHandler not found" );
        }
        */

        if( this.livechatapi )
        {
            logi( "livechatapi found" );
        }
        else
        {
            loge( "livechatapi not found" );
        }

        this.livechatapi.registerListener( "call-signal", ( content, from ) =>{
            this.processSigMessage( content, from );
        } );

        /*
        sdkHandler.subscribeEvent('cs', 'onEnd', ( content, from ) => {
            //this.End( "" );
        });
        */
    }


    __sendEndAv = ( error, reason ) =>
    {
        logi( "Sending endav with, errorCode : " + error.name + ", reason : " + reason );
        this.SendCallSignal( { type:'endav', errorCode:(error.name), reason:(reason) } );
    }


    End = ( reason ) =>
    {
        logi( "End invoked" );
        var err = this.EndAvErrorForState();
        this.appEndav( reason );
        this.__sendEndAv( err, reason );
    }


    Reconnect = () =>
    {
        logd( "reconnecting" );
        this.avconn.tryReconnect();
    }


    appEndav = ( reason ) =>
    {
        if( this.IsActive() )
        {
            this.avconn.endav( reason );

            if( this.IsRtcActive() )
            {
                this.endCallTimer();
            }
            else
            {
                logw( "Ending before connect" );
            }
        }
        else
        {
            logw( "Ending while not in av mode" );
        }

        this.currentState = this.StateEvent.ENDED;
    }

    sysEndav = ( reason ) =>
    {
        if( this.IsActive() )
        {
            this.avconn.endav( reason );
            if( this.IsRtcActive() )
            {
                this.endCallTimer();
                this.invoke( this.StateEvent.ENDED, reason );
            }
            else
            {
                logw( "End received before media connect" );
                this.invoke( this.CommonEvent.AVSTART_FAILED, reason );
            }
        }
        else
        {
            if( this.IsInCallNego() )
            {
                logw( "End received in call negotiation" );
                this.endRingingTimer();
                this.invoke( this.CommonEvent.CALLSTART_FAILED, reason );
            }
            else
            {
                logw( "End received at Unexptect state " + this.currentState.name );
            }
        }

        this.currentState = this.StateEvent.ENDED;
    }

    //
    // remoteUsrObj = { id:"", title:"", url:"", remoteAddr:"" }
    // 
    StartAV = ( mode, remoteUsrObj , direct ) =>
    {
        this.remoteUserId = (remoteUsrObj.remoteAddr);
        logi( "Starting " + mode + " call to " + this.remoteUserId );
        remoteUserName = (remoteUsrObj.title);
        global.contact.name = remoteUserName;

        this.mode = mode;

        /*
        if( mode == "screen" )
        {
            this.intToExt.screen = "video";
            this.extToInt.video = "screen";
        }
        */

        if( direct )
        {
            this.__startav_withperm( mode, ()=>{ this.avconn.tryReconnect } );
            return;
        }

        if( Platform.OS === 'ios' )
        {
            this.SendCallSignal( { type:'requestav', param: this.intToExt[mode] } );
            this.invoke( this.StateEvent.MEDIA_REQ_SENT, mode );
        }

        // android
        this.getAndroidPermissionForMode( mode ).then( (ret)=>
        {
            logi( "(0) Permission returned '" + ret + "' for " + mode );

            if( !ret ) return;

            this.SendCallSignal( { type:'requestav', param: this.intToExt[mode] } );
            this.invoke( this.StateEvent.MEDIA_REQ_SENT, mode );
        } );

    }

    // for internal use

    __startav_withperm=( extmode, postStartCall )=>{

        this.mode = this.extToInt[extmode];

        logi( "Setting configs " + JSON.stringify( global.configs.av ) );
        this.avconn.setConfig( JSON.stringify( global.configs.av ) );

        logd( "Calling initav, mode = " + this.mode );

        this.avconn.initav( 'test', this.mode, ( status, errMsg ) => {

            if( !status )
            {
                loge( "initav failed with '" + errMsg + "'" );
                return;
            }

            logd( "Calling startav" );

            if( Platform.OS === 'ios' )
            {
                this.__startav( this.mode );
                postStartCall();
                return;
            }

            // android
            this.getAndroidPermissionForMode( this.mode ).then( ( ret1 )=>
            {
                logi( "(2) Permission returned '" + ret1  + "' for " + this.mode );
                if( !ret1 ) return;
                this.__startav( this.mode );
                postStartCall();
            } );
            
        } );
    }

    __startav = ( mode ) =>
    {
        logi( "Staring av in mode " + mode );

        if( (mode != "audio" ) && ( mode != "video" ) && ( mode != "screen" ) )
        {
            loge( "unknown av mode " + mode );
            return;
        }

        this.avconn.onEnd = ( type, desc )=>
        {
            logi( "onEnd triggered" );
	        this.avconn.endav( "CSonEnd:" + type );
            this.invoke( this.StateEvent.ENDED, (type + " - " + desc) );
            this.__sendEndAv( this.Errors.CALL_DISCONNECTED_ABRUPTLY, "--" + (type + " - " + desc) );
        }

        this.avconn.onSignallingMessage = ( message ) =>
        {
            var jobj = JSON.parse( message );
            if( jobj.type == "candidate" )
            {
                this.localIceDetected = true;
            }
            this.SendCallSignal( jobj );
        }

        this.avconn.onVideo = ( index, side, streamId ) =>
        {
            var map = this.remoteStreamMap[streamId];
            var sourceType =  map ? map.type : undefined;
            var owner = map ? map.owner : undefined;

            this.invoke( this.CommonEvent.VIDEO_RECEIVED, index, side, sourceType, owner );
        }

        this.avconn.onVideoDrop = ( index, side, streamId )=>
        {
            var map = this.remoteStreamMap[streamId];
            var sourceType =  map ? map.type : undefined;
            var owner = map ? map.owner : undefined;

            logi( "video dropped, for stream " + streamId + ", of type = " + sourceType );
            this.invoke( this.CommonEvent.VIDEO_DROPPED, index, side, sourceType, owner );

            delete this.remoteStreamMap[streamId];
        }

        this.avconn.onMediaDisconnect = () =>
        {
            this.invoke( this.StateEvent.RECONNECTING_MEDIA, this.mode );
        }

        this.avconn.onMediaReconnect = () =>
        {
            this.invoke( this.StateEvent.REINCALL_MEDIA, this.mode );
        }

        this.avconn.onMediaReconnectableEnd = () =>
        {
            this.invoke( this.StateEvent.MEDIA_RECONNECTABLE, this.mode );
        }

        var onCompleteFunc = ( status, errmsg ) => {

            if( status )
            {
                logi( "onSuccess triggered" );
                this.invoke( this.StateEvent.INCALL_MEDIA, mode );
                this.startCallTimer();
                this.performRetainedActions();
            }
            else
            {
                logw( "onFail triggered, err : " + errmsg );
		this.avconn.endav( "CSstartFailed:" + errmsg );

                this.invoke( this.CommonEvent.AVSTART_FAILED, errmsg );

                var errCode = ( ! this.localIceDetected ) ? this.Errors.ICE_NOT_FOUND :
                   ( ! this.remoteIceReceived ) ? this.Errors.ICE_NOT_RECEIVED  : this.Errors.ICE_NOT_CONNECTED;

                this.__sendEndAv( errCode, "--" + errmsg );
                this.currentState = this.StateEvent.ENDED;
            }
        };

        logd( "calling startav" );

        this.avconn.startav( 'test', onCompleteFunc , this.mode );

        this.invoke(  this.StateEvent.CONNECTING_MEDIA, mode );

    }

    performRetainedActions = () =>
    {
        logi( "performRetainedActions()" );

        if( this.isLoud != undefined  ) 
        {
            logi( "Calling retained 'LoudSpeaker'" + this.isLoud );
            this.LoudSpeaker( this.isLoud );
        }
        if( this.isMute != undefined )
        {
            logi( "Calling retained 'Mute'" + this.isMute );
            this.Mute( this.isMute );
        }
        if( this.hasVideo != undefined ) 
        {
            logi( "Calling retained 'VideoEnabled'" + this.hasVideo );
            this.VideoEnabled( this.hasVideo );
        }
    }

    getAndroidPermissionForMode = async ( mode ) => {

        try {

            var granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
              {
                'title': 'Permission to record from mic',
                'message': "Tetherfi livechat need permission to record your voice mic"
              }
            )
            if ( granted != PermissionsAndroid.RESULTS.GRANTED ) {
              console.log("Mic permission denied")
              return false;
            }

            if( mode == "video" )
            {
                granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                      'title': 'Permission to camera',
                      'message': "Tetherfi livechat need permission to capture video from your camera"
                    }
                  )
                  if ( granted != PermissionsAndroid.RESULTS.GRANTED ) {
                    console.log("Camera permission denied")
                    return false;
                  }
            }

            return true;

          } catch (err) {
            console.warn(err)
            return false;
          }
    }


    Mute = (enable) =>
    {
        this.isMute = enable;
        if( !this.IsRtcActive() )
        {
            logi( "Mute called while not in AV, value (" + enable + ") retained" );
            return;
        }
        this.avconn.micMute( enable );
    }


    LoudSpeaker = (enable)=>
    {
        this.isLoud = enable;
        if( !this.IsRtcActive() )
        {
            logw( "Speaker called while not in AV, value (" + enable + ") retained *" );
            return;
        }
        logd( "Setting speaker mode = " + enable + " *" );
        this.avconn.speakerPhone( enable );
    }

    VideoEnabled = ( enable ) =>
    {
        this.hasVideo = enable;

        if( !this.IsRtcActive() )
        {
            logw( "Video enabled called while not in AV, value (" + enable + ") retained" );
            return;
        }

        if( enable )
            this.avconn.resumeVideo( true );
        else
            this.avconn.pauseVideo( false );
    }


    processSigMessage ( content, from ) // Livechat2 message content
    {
        logi( "Sig message received from : " + from );

        var jobj = JSON.parse( content );

        logi( "type = " + jobj.type );

        var reqHandle = global.configs.av.prompt_avrequest;

        if( jobj.type === "requestav" )
        {
            this.callid = jobj.callId;
            //var arr = from.split(":");
            
            this.mode = this.extToInt[jobj.param];
            
            this.remoteUserId = from;
            this.SendCallSignal( { type:"ack-requestav", param:jobj.param } );

            if( this.IsActive() )
            {
                logw( 'requestav received while a call active, requestav param : ' + jobj.param + ' request rejected' );
                this.SendCallSignal( { type:"avtstatus", param: 'false', errorCode: this.Errors.CALL_INPROGRESS.name } );
                return;
            }
            
            var arr = from.split(":");
            var user = (arr.length > 1 ) ? arr[1] : arr[0];

            if( this.useridResolver )
            {
                logd ( "Resolver found" );
                this.remoteUserName = this.useridResolver( user );
            }

            if( ! this.remoteUserName )
            {
                logd ( "No username set" );
                this.remoteUserName = "Unknown";
            }

            logi ( "Resolved name of the user : " + this.remoteUserName );
            global.contact.name = this.remoteUserName;
        
            this.invoke( this.StateEvent.MEDIA_REQ_RECEIVED, jobj.param , jobj.offering, this.callid, this.remoteUserName  );

            this.startRingingTimer(); // will be cleared in ProcessUserAvResponse

            if( reqHandle == 0 )
            {
                this.ProcessUserAvResponse(jobj.param);
            }
            else if( reqHandle == 1 )
            {
                Alert.alert(
                    'Requesting ' + jobj.param,
                    'Remote party is offering ' + jobj.offering,
                    [
                        {text: 'Start', onPress: () => this.ProcessUserAvResponse(jobj.param) },
                        {text: 'Cancel', onPress: () => this.ProcessUserAvResponse('false', this.Errors.CALL_REJECTED ) },
                    ],
                    { cancelable: false }
                );
            }
            else if ( reqHandle == 2 )
            {
                // Application will handle MEDIA_REQ_RECEIVED event and 
                // call ProcessUserAvResponse( 'false' | 'true', 'audio' | 'video' )
                // or this.ringingTimer will invoke this._sendAvResponse( false, this.Errors.CALL_NOT_ANSWERED );
                return;
            }
            return;
        }

        if( jobj.type === "requestav-callid"  )
        {
            this.callid = jobj.callId;
            this.remoteUserId = from;
            return;
        }

        if(  jobj.type === "ack-requestav"  )
        {
            this.callid = jobj.callId;

            this.remoteUserId = from;
            this.invoke( this.StateEvent.MEDIA_REQ_ACKED );
            return;
        }

        if (jobj.type === "join-requestav")
        {
            //TO-DO: Add support for join-requestav 
            logw(`Unsupported join request received for callid: ${jobj.callId}`);
            return;
        }

        if( this.callid != jobj.callId )
        {
            loge( "unexpected callid :" + jobj.callId + ", expected : " + this.callid );
            return;
        }

        if( jobj.type === "call-state" )
        {
            logd( "'call-state' event invoked, with " + jobj.callState + " - " + jobj.userId );
            this.invoke( this.CommonEvent.CALL_STATE, jobj.callState, jobj.userId );
            return;
        }

        
        if( jobj.type === "avtstatus" )
        {
            if( jobj.param === 'false' )
            {
                this.invoke( this.CommonEvent.CALLSTART_FAILED, 
                    jobj.errorCode ? 
                    jobj.errorCode : this.Errors.CALL_REJECTED.name );
                    
                return;
            }

            this.remoteUserId = from;

            this.__startav_withperm( jobj.param, this.avconn.tryReconnect );

            return;
        }

        if( (jobj.type == "endav" ) || ( jobj.type == 'bye' ) )
        {
            var reason = (jobj.reason == undefined) ? "REMOTE_END_REQUESTED" : jobj.reason ;
            this.sysEndav( reason ); // Changes the state to END
            return;
        }

        if( jobj.type == "avcontrol" )
        {
            if( jobj.value == "muteaudio" )
                this.avconn.pause();
            else if( jobj.value == "unmuteaudio" )
                this.avconn.resume();
            else
                logi( "warn", "unexpected" );

            return;
        }

        // === Nuwan - Begining of new changes

        if( (jobj.type == "addscreenshare") || ( jobj.type == "requestscreenshare" ) )
        {
            if( !this.IsActive() )
            {
                loge( 'Unexpected request to share the screen when there is no call' );
                this.SendCallSignal( 
                    { 
                        type:"screensharestatus", 
                        param: "false", 
                        errorCode : this.Errors.CALL_NOT_AVAILABLE.name 
                    } 
                );
                return;
            }

            var arr = from.split(":");
            var userid = (arr.length > 1 ) ? arr[1] : arr[0];
            var username = this.useridResolver ? this.useridResolver( user ) : undefined;

            if( this.useridResolver )
            {
                logd ( "Resolver found" );
                this.remoteUserName = this.useridResolver( user );
            }

            if( jobj.type == "addscreenshare" )
            {
                this.invoke(  this.CommonEvent.SS_PERM_REQ_RECEIVED, "addscr", this.callid, userid, username );
                if( reqHandle == 0 )
                {
                    this.SendCallSignal( {  type:"screensharestatus", param: "true" } );
                }
                else if( reqHandle == 1 )
                {
                    Alert.alert(
                        'Permission to share screen',
                        'Remote user requested to share his/her screen',
                        [
                            {text: 'Allow', onPress: () => this.ProcessUserScreenResponse( jobj.type, true ) },
                            {text: 'Reject', onPress: () => this.ProcessUserScreenResponse( jobj.type, false ) }
                        ],
                        { cancelable: false }
                    );
                }
                else if ( reqHandle == 2 )
                {
                    // Application will handle SS_PERM_REQ_RECEIVED event and 
                    // call ProcessUserScreenResponse( 'addscreenshare' | 'requestscreenshare', true | false )
                    // TODO : use this.ringingTimer also to cancel this
                    return;
                }
            }
            else if ( jobj.type == "requestscreenshare" )
            {
                this.invoke( this.CommonEvent.SS_REQUEST_RECEIVED, "requestscr", this.callid, userid, username );
                if( reqHandle == 0 )
                {
                    this.SendCallSignal( {  type:"screensharestatus", param: "true" } );
                }
                else if( reqHandle == 1 )
                {
                    Alert.alert(
                        'Request to share remote screen',
                        'Remote user is requesting to share your screen',
                        [
                            {text: 'Share', onPress: () => this.ProcessUserScreenResponse( jobj.type, true ) },
                            {text: 'Reject', onPress: () => this.ProcessUserScreenResponse( jobj.type, false ) }
                        ],
                        { cancelable: false }
                    );
                }
                else if ( reqHandle == 2 )
                {
                    // Application will handle SS_REQUEST_RECEIVED event and 
                    // call ProcessUserScreenResponse( 'addscreenshare' | 'requestscreenshare', true | false )
                    // TODO : use this.ringingTimer also to cancel this
                    return;
                }
            }
            else
            {
                loge( "Unknown screenshare type '" + this.type + "'" );
            }

            return;
        }

        if( jobj.type == "screensharestatus" )
        {
            if( jobj.param == "true" )
            {
                // add ss
            }
            else if( jobj.param == "false" )
            {
                this.invoke( this.CommonEvent.SS_START_FAILED, "err" )
            }
            return;
        }

        if( jobj.type == "endscreenshare" )
        {
            return;
        }

        if( jobj.type == "screensharestart-notification" )
        {
            return;
        }

        // === Nuwan - End of new changes

        if( ( jobj.type == "offer" ) || ( jobj.type == "answer" ) )
        {
            if( jobj.streamInfo )
            {
                this.updateStreamMap( jobj.streamInfo );
            }
            else 
            {
                logw( "'streamInfo' object is missing in '" + jobj.type + "'" );
            }
        } 
        else if( jobj.type == "candidate" )
        {
                this.remoteIceReceived = true;
        }
        
        this.avconn.passSigMessage( content );

    }

    ProcessUserScreenResponse = ( sc_mode /* "ADDSC", "REQSC" */, bResp /* bool true or false */ ) =>
    {
        if( sc_mode.startsWith( "addscr" ) )
        {
            this.SendCallSignal( {  type:"screensharestatus", param: bResp.toString() } )
        }
        else if( sc_more.startsWith( "requestscr" ) )
        {
            // start local SS
        }
        else
        {
            logw( "Unknown SC mode " + sc_mode );
        }
    }

    ProcessUserAvResponse=( resp ) =>
    {
        if( this.endRingingTimer() )
        {
            logw( "User response received aftre ringing timeout" );
            return;
        }

        if( this.currentState == this.StateEvent.ENDED )
        {
            logw( "Call canceled while wating for user-response" );
            return;
        }

        if( resp == "false" )
            this._sendAvResponse( resp, this.Errors.CALL_REJECTED );
        else
        {
            if( resp.startsWith("screen") )
            {
                resp = "audio";
            }
            this._sendAvResponse( resp );
        }
    }

    _sendAvResponse=(resp , err ) =>
    {
        var jobj = { type:"avtstatus", param:resp };

        if( err )
        {
            jobj.errorCode = err.name;
        }

        if( resp === "false" )
        {
            logi( "av request canceled by user" );
            this.currentState = this.StateEvent.ENDED;
            this.SendCallSignal( jobj );
            return;
        }

        if( Platform.OS === 'ios' )
        {
                this.__startav_withperm( resp, ()=>{} );
                this.SendCallSignal( jobj );
                return;
        }

            // android
        this.getAndroidPermissionForMode( this.mode ).then( 
            ( ret1 ) => {
                logi( "(1) Permission returned '" + ret1  + "' for " + this.mode  );
                if( ret1 ){
                    this.__startav_withperm( resp, ()=>{} );
                } else {
                    jobj.param = 'false';
                }
                this.SendCallSignal( jobj );
            }
        );
    }

    SendCallSignal = ( callsigObject ) =>
    {
        callsigObject.callId = this.callid;
        callsigObject.owner = "user:" + this.userid;

        if( this.intToExt[this.mode] == "screenshare" )
        {
            callsigObject.__localId = "0";
            callsigObject.isvideo = true;
            callsigObject.isscreenshare = true;
        }

        logi( "Sig message type: '" + callsigObject.type + "' sending out to : " + this.remoteUserId );
        this.livechatapi.sendCallSignal( JSON.stringify( callsigObject ), /*"user:" +*/ this.remoteUserId );
    }

    startCallTimer = () =>
    {
        this.callstartTime = Date.now();
        this.calltimer = setInterval( ()=>
        {
            var deltasec = ( (Date.now() - this.callstartTime)/1000 );
            this.formatedCallTime = Math.floor( deltasec/60 ) + ":" + (deltasec%60);
            this.invoke( this.CommonEvent.CALL_TIME, this.formatedCallTime, deltasec );

        }, 1000 );
    }

    endCallTimer = () =>
    {
        clearInterval( this.calltimer );
    }

    startRingingTimer = () =>
    {
        this.ringingTimer = setTimeout( ()=>
        {
            this.ringingTimer_invoked = true;
            this.invoke( this.StateEvent.MEDIA_REQ_TIMEDOUT );
            this._sendAvResponse( "false", this.Errors.CALL_NOT_ANSWERED );

        }, 30000 );

        this.ringingTimer_invoked = false;
    }

    endRingingTimer = () =>
    {
        clearTimeout( this.ringingTimer );
        return this.ringingTimer_invoked; 
    }

    updateStreamMap = ( newStreamMap ) =>
    {
        for( var rStreamId in this.remoteStreamMap )
        {
            var rStream = newStreamMap[ rStreamId ];

            if( rStream )
            {
                // an existing stream, nothing new to do
            }
            else
            {
                logi( "Sdp stream dropped ( " + rStreamId + " - " + this.remoteStreamMap[rStreamId].type  + " )" );
                // an existing stream Dropped
                // fire event for stream ( rStreamId, rStream.type ) drop
            }
        }

        for ( var newRStreamId in newStreamMap )
        {
            var newRStream = this.remoteStreamMap[ newRStreamId ];

            if( newRStream )
            {
                // an existing stream, nothing new to do
            }
            else
            {
                logi( "Sdp stream added ( " + newRStreamId + " - " + newStreamMap[newRStreamId].type  + " )" );
                this.remoteStreamMap[newRStreamId] = newStreamMap[newRStreamId]
                // new stream found 
                // fire event for new stream ( newRStreamId, newRStream.type ) detected 
            }
        }

        //this.remoteStreamMap = newStreamMap;

    }
}


export class CallSessionConfig
{
    constructor( sigurl, custinfo )
    {
        this.SigUrl = sigurl;
        this.CustInfo = custinfo;
    }

    SigUrl;
    CustInfo;
}

