
import { NativeModules, NativeEventEmitter } from 'react-native';

const { RNLivechatav } = NativeModules;

export default class AVChatConnection 
{
    constructor()
    {
        this.eventSubs = [];

        this.eventEmitter = new NativeEventEmitter( RNLivechatav );

        var sub;

        sub = this.eventEmitter.addListener( "sigmsg", (event)=>{
            console.log( "livechat: " + event.msg );
            this.onSignallingMessage( event.msg );
        } );

        this.eventSubs.push( sub );

        sub = this.eventEmitter.addListener( "logmsg", (event)=>{
            this.onLogMessage( event.type, event.class, event.sid, event.msg, event.time );
        } );

        this.eventSubs.push( sub );

        sub = this.eventEmitter.addListener( "onend", (event)=>{
            this.onEnd( event.type, event.desc );
        } );

        this.eventSubs.push( sub );

        sub = this.eventEmitter.addListener( "onvideo", (event)=>{
            this.onVideo( event.index, event.type, event.streamId );
        } );

        this.eventSubs.push( sub );

        sub = this.eventEmitter.addListener( "onvideodrop", (event)=>{
            this.onVideoDrop( event.index, event.type, event.streamId );
        } );

        this.eventSubs.push( sub );

        sub = this.eventEmitter.addListener( "ondisconnect", (event)=>{
            this.onMediaDisconnect(  );
        } );

        this.eventSubs.push( sub );

        sub = this.eventEmitter.addListener( "onreconnect", (event)=>{
            this.onMediaReconnect( );
        } );

        this.eventSubs.push( sub );

        sub = this.eventEmitter.addListener( "onreconnend", (event)=>{
            this.onMediaReconnectableEnd( );
        } );

        this.eventSubs.push( sub );

        console.log( "Subscribed to '" + this.eventSubs.length + "'" );
    }

    release = () =>
    {
        this.onLogMessage( "debug", "livechatav", "", "Releasing handlers", "" );
        this.eventSubs.forEach( function( sub ){ sub.remove(); }  );
        this.eventSubs = [];
    }

    onSignallingMessage = (message)=>{};
    onEnd = ( type, desc ) =>{}
    onVideo = ( index, event, streamId ) =>{}
    onVideoDrop = ( index, event, streamId )=>{}
    onMediaDisconnect = () => {}
    onMediaReconnect = () => {}
    onMediaReconnectableEnd = () => {}

    onLogMessage = ( type, class1, sid, msg, time )=>{
        try
        {
            global.log.func( type, class1+": "+ msg );
        }
        catch(err)
        {
        }

        // console.log( type + "," + class1 + "," + msg + "," );

    };

    initav( sid, mode, onComplete )
    {
        RNLivechatav.initav( sid, mode, onComplete );
    }

    startav( sid, onComplete, mode )
    {
        RNLivechatav.getDeviceInfo( (msg)=>
        {  
            this.onSignallingMessage( '{ "type":"devinfo", "info":' + msg + ' }' );
        } );

        RNLivechatav.startav( sid, mode, onComplete ); // can not convert non-fn arg after fn arg
        
    }

    pauseVideo( withRemote )
    {
        RNLivechatav.pauseVideo( withRemote, true );
    }

    resumeVideo( withRemote )
    {
        RNLivechatav.resumeVideo( withRemote, true );
    }

    tryReconnect()
    {
        RNLivechatav.tryReconnect();
    }

    passSigMessage( message )
    {
        RNLivechatav.passSigMessage( message );
    }

    endav( reason )
    {
        RNLivechatav.endav( reason );
    }

    setConfig( configs )
    {
        RNLivechatav.setConfig( configs );
    }

    pause()
    {
        RNLivechatav.pause();
    }

    resume()
    {
        RNLivechatav.resume();
    }

    micMute( doMute )
    {
        RNLivechatav.muteMic( doMute );
    }

    speakerPhone( enable )
    {
        RNLivechatav.speakerPhone( enable );
    }

    playAudio()
    {
        RNLivechatav.playAudio();
    }

    stopAudio()
    {
        RNLivechatav.stopAudio();
    }

    // onLogMessage( type, class, msg, time )

    enableLogEvents( val )
    {
        RNLivechatav.enableLogEvents( val );
    }

    echo( message, onEcho )
    {
        RNLivechatav.echo( message, onEcho );
    }

    setAppName(name)
    {
        RNLivechatav.setAppName(name);
    }

    static getLibVersion( onVer )
    {
        RNLivechatav.getLibVersion( onVer )
    }

    getAppName()
    {
        return RNLivechatav.getAppName();
    }

    getui(onview)
    {
        console.log( "getui" );
        RNLivechatav.getui(onview);
    }

}

/*

export default class AVChatConnection 
{
    onSignallingMessage = (message)=>{};

    onLogMessage = ( type, class1, msg, time )=>{};

    startav( sid, onComplete ){
        
        this.onSignallingMessage( '{ "type":"devinfo", "info":"--" }' );
    }

    tryReconnect(){}

    passSigMessage( message ){}

    endav( reason ){}

    setConfig( configs ){}

    micMute( doMute ){}

    speakerPhone( enable ){}

    pause(){}

    release(){}

    resume(){}

    playAudio(){}

    stopAudio(){}

    enableLogEvents( val ){}

    echo( message, onEcho ){}

    setAppName(name){}

    getAppName(){}

    static getLibVersion( onVer )
    {
        setTimeout( function(){ onVer("test_ver_1.0"); }, 100 );
    }

    getui(onview){
        console.log( "getui" );
        onview( null );
    }

    onSignallingMessage = (message)=>{};
    onEnd = ( type, desc ) =>{}
    onVideo = ( videoview ) =>{}
    onMediaDisconnect = () => {}
    onMediaReconnect = () => {}
    onMediaReconnectableEnd = () => {}

}

*/

