
package com.reactlibrary;

import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.bridge.JavaScriptModule;

import sdk.livechat.Configs;

public class RNLivechatavPackage implements ReactPackage {

    /*

    Not used since RN 0.60

    NativeConfigs nconfig;
    public void setNativeConfigs( NativeConfigs config )
    {
        nconfig = config;
        Configs.Instance.setPauseAudio( nconfig.PauseMusicRsid );
    }
    */

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
      return Arrays.<NativeModule>asList(new RNLivechatavModule(reactContext));
    }

    // Deprecated from RN 0.47
    public List<Class<? extends JavaScriptModule>> createJSModules() {
      return Collections.emptyList();
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext)
    {
        return Arrays.<ViewManager>asList( RNVideoViewManager.Instance );
    }

}
