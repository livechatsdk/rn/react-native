package com.reactlibrary;


import android.view.SurfaceView;


import com.facebook.react.uimanager.LayoutShadowNode;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;

import com.facebook.react.uimanager.annotations.ReactProp;

import sdk.livechat.Log;

public class RNVideoViewManager extends SimpleViewManager<RNVideoView>
{
    LayoutShadowNode shadowNode;
    public static RNVideoViewManager Instance = new RNVideoViewManager();

    public RNVideoViewManager()
    {
        Log.i( "Video manager created" );
    }

    SurfaceView[] viewlist = new SurfaceView[10];
    int nextIndex = 0;

    @Override
    public String getName()
    {
        return "RNVideoView";
    }

    public void reset()
    {
        for (int i = 0; i < viewlist.length; i++) {
            viewlist[i] = null;
        }
        nextIndex = 0;
    }

    @Override
    public LayoutShadowNode createShadowNodeInstance()
    {
        return shadowNode = super.createShadowNodeInstance();
    }

    @Override
    protected RNVideoView createViewInstance(ThemedReactContext reactContext)
    {
        Log.i( "createViewInstance() invoked" );
        RNVideoView vcon = new RNVideoView( reactContext );
        return vcon;
    }

    @ReactProp(name = "viewindex", defaultInt = -1 )
    public void setViewIndex( RNVideoView view, int index )
    {
        Log.i( "setting viewindex to " + index );

        if( index < 0 )
        {
            Log.d( "negative value, ignored" );
            return;
        }

        SurfaceView sf = viewlist[index];

        if( sf == null )
        {
            Log.e( "Surfaceview is null" );
            return;
        }


        Log.i( "SurfaceView view found" );

        view.setSurfaceView( sf );
    }

    public int addSurfaceView( SurfaceView sfview, String type )
    {
        Log.d( "SurfaceView added to the list, type:" + type );
        int i = nextIndex++;
        viewlist[i] = sfview;
        return i;
    }

    public int removeSurfaceView( SurfaceView sfview )
    {
        int index = -1;
        Log.d( "Removing surface view" );
        for (int i = 0; i < viewlist.length; i++)
        {
            if( viewlist[i] == sfview )
            {
                index = i;
                viewlist[i] = null;
                break;
            }
        }

        if( index == -1 )
        {
            Log.w( "In removing, couldn't find surface in the list" );
        }

        return index;

    }

}

