import axios from 'axios';
import {Network} from './index';
import {timeOut} from './../constants/config';

const doRequest = async (
  apiUrl,
  method,
  requestPayLoad = {},
  responseCallBack,
) => {
  method = method.toLowerCase();
  console.log('API_REQUEST_POST_REQUEST:', apiUrl, requestPayLoad);
  const networkConnectivity = await Network.getNetworkConnectivity();

  if (networkConnectivity) {
    await axios({
      method: method === '' ? 'post' : method,
      timeout: timeOut,
      url: apiUrl,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      data:
        method === 'post' || method === 'put' || method === 'delete'
          ? requestPayLoad
          : null,
    })
      .then(response => {
        console.log('API_REQUEST_RESPONSE:', JSON.stringify(response));
        responseCallBack({
          status: true,
          message: 'SUCCESS',
          data: response,
        });
      })
      .catch(error => {
        console.log('API_REQUEST_ERROR:', error.message);
        responseCallBack({
          status: false,
          message: error.message,
          data: null,
        });
      });
  } else {
    responseCallBack({
      status: false,
      message: 'Check network availability',
      data: null,
    });
  }
};

export default {
  doRequest,
};
