const doRequest = async (
  apiUrl,
  method,
  requestHeaders = {},
  requestPayLoad = undefined,
) => {
  method = method.toUpperCase();
  console.log('HTTP_REQUEST:', apiUrl, requestHeaders, requestPayLoad);

  requestHeaders['Accept'] = '*/*';
  requestHeaders['Content-Type'] = 'application/json';

  try {
    let res = await fetch(apiUrl, {
      method: method,
      headers: requestHeaders,
      body: requestPayLoad,
    });
    return {status: true, body: await res.json(), headers: res.headers};
  } catch (e) {
    return {status: false, error: e.message, stack: e.stack};
  }
};

const doFileUpload = async (
  apiUrl,
  requestHeaders = {},
  requestPayLoad = undefined,
) => {
  console.log('HTTP_FILE_REQUEST:', apiUrl, requestHeaders, requestPayLoad);

  //requestHeaders['Accept'] = '*/*';
  //requestHeaders['Content-Type'] = 'multipart/form-data';

  try {
    let options = {
      method: 'POST',
      headers: requestHeaders,
      body: new FormData(),
    };

    for (let key in requestPayLoad) {
      options.body.append(key, requestPayLoad[key]);
    }

    let res = await fetch(apiUrl, options);
    return {status: true, body: await res.json(), headers: res.headers};
  } catch (e) {
    return {status: false, error: e.message, stack: e.stack};
  }
};

export default {
  doRequest,
  doFileUpload,
};
