import {storeItem, retrieveItem, clear} from './LocalStorage';

const TOKEN_KEY = '@@user';
const LOGGED_USER = '@@logged_user';
const LOGGED_USER_PASSWORD = '@@logged_user_password';
const LOGGED_USER_ID = '@@logged_user_id';
const LOGGED_USER_TENANTTOKEN = '@@logged_user_TenantToken';
const workspaceKey = '@@organization---';
const workspacesKey = '@@organizations---';
const refreshTokenKey = '@@@refreshToken-----';
import jwt_decode from 'jwt-decode';

const SECRET_KEY = 'my secret key';

//import { __APP_IDENTITY_PROVIDER__ } from "../config/baseUrls";

import {configs} from './../config'

export const setToken = async token => {
  //console.log('login token....');
  //console.log(token);
  try {
    //secureStorage.setItem(TOKEN_KEY, token);
    let res = await storeItem(TOKEN_KEY, token);
    console.log('JAY', 'SET_TOKEN', res);
    const details = jwt_decode(token);


    if ( configs.selected.__APP_IDENTITY_PROVIDER__ === 'GUARDIAN') {
      await storeItem(LOGGED_USER_ID, details.id);
      await storeItem(LOGGED_USER_TENANTTOKEN, token);

    } else {
      const sub = JSON.parse(details.sub);
      if (sub) {
        await storeItem(LOGGED_USER_ID, sub.userId);
        await storeItem(LOGGED_USER_TENANTTOKEN, sub.tenantToken);
      }
    }
  } catch (ex) {
    console.error('JAY setToken', ex);
  }
};

export const setUser = async username => {
  await storeItem(LOGGED_USER, username);
};

export const getUser = async () => {
  // console.log('get token....');
  const user = await retrieveItem(LOGGED_USER);
  return user;
};

export const setPassword = async password => {
  await storeItem(LOGGED_USER_PASSWORD, password);
};

export const getPassword = async () => {
  // console.log('get token....');
  const user = await retrieveItem(LOGGED_USER_PASSWORD);
  return user;
};

export const getUserId = async () => {
  // console.log('get token....');
  const user = await retrieveItem(LOGGED_USER_ID);
  return user;
};

export const getTenantToken = async () => {
  // console.log('get token....');
  const user = await retrieveItem(LOGGED_USER);
  return user;
};

export const getToken = async () => {
  // console.log('get token....');
  const token = await retrieveItem(TOKEN_KEY);
  return token;
};

export const getBearerToken = async () => {
  // console.log('get token....');
  const token = await retrieveItem(TOKEN_KEY);
  return `Bearer ${token}`;
};

export const setRefreshToken = async (refreshToken, userName) => {
  await retrieveItem(refreshTokenKey, JSON.stringify({refreshToken, userName}));
};

export const getRefreshToken = async () => {
  return JSON.parse(await retrieveItem(refreshTokenKey));
};

export const setWorkspaces = async organizations => {
  await storeItem(workspacesKey, JSON.stringify(organizations));
};

export const getWorkspaces = async () => {
  return JSON.parse(await retrieveItem(workspacesKey));
};

export const setWorkspace = async (organization, userName) => {
  await storeItem(workspaceKey, JSON.stringify({organization, userName}));
};

export const getWorkspaceData = async () => {
  return JSON.parse(await retrieveItem(workspaceKey));
};

export const getWorkspace = async () => {
  const wdata = JSON.parse(await retrieveItem(workspaceKey));
  return wdata ? wdata.organization : null;
};

export const userLogOut = async () => {
  console.log('remove token....');
  await clear();
};

export const isLogin = () => {
  //console.log('login....');
  const token = getToken();
  if (token) {
    return true;
  }
  return false;

  /* if (token) {
            let decoded = jwt.verify(token, "tetherfi");
            if (decoded && decoded.sub && decoded.iat) {
              return true;
            } else {
              userLogOut();
              console.log("User cant be access main console : logout");
              return false;
            }
          } */
};

export const getLoggedInUser = () => {
  /* let token = getToken();
          let decoded = jwt.decode(token);

          if (decoded) {
            return decoded.sub;
          } else {
            return null;
          } */

  let user = getUser();
  return user;
};

export const userDetails = () => {
  //console.log('get  user details....');
  const token = getToken();
  if (token) {
    console.log(jwt_decode(token));
    return jwt_decode(token);
  }
};
/*
export const getUserId = () => {
  //console.log('get  user details....');
  const token = getToken();
  if (token) {
    let decoded = jwt.decode(token);
    console.log(decoded);
    if (decoded.sub) {
      return JSON.parse(decoded.sub).userId;
    } else {
      return "";
    }
  }
};
 */
