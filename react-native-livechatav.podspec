require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-livechatav"
  s.version      = "1.0.0"
  s.summary      = "RNLivechatav"
  s.description  = <<-DESC
                  react-native-livechatav
                   DESC
  s.homepage     = "https://github.com/github_account/react-native-livechatav"
  s.license      = "MIT"
  # s.license    = { :type => "MIT", :file => "FILE_LICENSE" }
  s.authors      = { "Nuwan" => "nuwan@tetherfi.com" }
  s.platforms    = { :ios => "9.0" }
  s.source       = { :git => "https://github.com/github_account/react-native-livechatav.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true
  s.vendored_frameworks = "ios/livechatav.framework", "ios/WebRTC.xcframework"
  s.static_framework = true

  s.dependency "React"
  # ...
  # s.dependency "..."
end

