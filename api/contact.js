import {Http} from './../utils';

//import {contactServiceUrl} from './../config/baseUrls';

import {configs} from './../config';

export const getContactList = async (user_id, organization, token) => {
  return await Http.doRequest(
    `${configs.selected.contactServiceUrl}/presence/user/${user_id}/${organization}`,
    'GET',
    {authorization: `Bearer ${token}`},
  );
};
