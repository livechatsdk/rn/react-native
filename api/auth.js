import {Http} from './../utils';
//import {userUrl} from './../config/baseUrls';
import {configs} from './../config'


export const getUserDetails = async (user_id, token) => {
  return await Http.doRequest(
    `${configs.selected.userUrl}/user-auth-service/api/v1/user/account/${user_id}`,
    'GET',
    {authorization: token},
  );
};

export const getUserList = async (authToken, tenantToken, uidArr ) => {
  return await Http.doRequest(
    //`${userUrl}/user-auth-service/api/v1/user/accounts?pageNo=1&pageSize=1000`,
    `${configs.selected.userUrl}/user-auth-service/api/v1/user/accounts`,
    'POST',
    {authorization: authToken, 'tenant-token': tenantToken},
    JSON.stringify( uidArr )
  );
};
