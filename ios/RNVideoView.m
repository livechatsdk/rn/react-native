//
//  RNVideoView.m
//  RNLivechatav
//
//  Created by Nuwan Abeysinghe on 5/1/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "RNVideoView.h"

#import <Log.h>

static int viewCount = 0;

@interface RNVideoView()
{
    UIView* videoView;
    int logid;
}

@end

@implementation RNVideoView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)init
{
    self = [super init];
    logid = (viewCount++);
    logip( @"RNVideoView(%d) created", logid );
    //[self setBackgroundColor:UIColor.blueColor];
    return self;
}

- (void)layoutSubviews
{
    logdp( @"(%d) RNView size %f, %f ", logid , self.bounds.size.width, self.bounds.size.height );
    [super layoutSubviews];
}

- (void)updateConstraints
{
    logdp( @"(%d) Updating constraints", logid );
    
    if( videoView )
    {
        videoView.frame = self.bounds;
           /*
        dispatch_async( dispatch_get_main_queue() , ^{
               videoView.bounds = self.bounds;//CGRectMake(0, 0, myside.width, myside.height );
        });
        */
    }
    
    [super updateConstraints];
    
    /*
    [videoView addConstraints:
     [NSArray arrayWithObjects:
      [NSLayoutConstraint constraintWithItem:videoView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1 constant:-2],
      [NSLayoutConstraint constraintWithItem:videoView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:1 constant:-2],
      nil
      ]
     ];
     */
}

-(void)setVideoView:(UIView *)view
{
    if( videoView != nil )
    {
        logwp( @"(%d) a videoview already exist, replacing", logid );
    }
    
    videoView = view;
    
    videoView.frame = self.bounds; // CGRectMake(0, 0, myside.width, myside.height );
    logdp( @"(%d) Setting video frame to RNView size %f, %f ", logid , self.bounds.size.width, self.bounds.size.height );
    
    videoView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight );
    
    [self setContentMode:UIViewContentModeScaleAspectFit];
    [self setContentMode:UIViewContentModeCenter];
    
    [self setAutoresizesSubviews:YES];
    
    [self addSubview:videoView];
    
    videoView.center = CGPointMake(self.frame.size.width  / 2,
    self.frame.size.height / 2);
    
    [self setNeedsUpdateConstraints];
    
    logip( @"(%d) Video view added*", logid );
}


@end
