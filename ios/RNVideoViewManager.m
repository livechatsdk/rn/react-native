//
//  RNVideoViewManager.m
//  RNLivechatav
//
//  Created by Nuwan Abeysinghe on 31/12/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "RNVideoViewManager.h"
#import "RNVideoView.h"

#import <Log.h>

static RN_VideoViewManager* _instance = nil;

@interface RNVideoViewManager : RCTViewManager

- (UIView *)view;

@end

@implementation RNVideoViewManager

RCT_EXPORT_MODULE();

- (UIView *)view
{
    logip( @"creating view %@", ((_instance == nil) ? @"nil" : @"not nil" )  );
    return [[RN_VideoViewManager Instance] createView];
}

RCT_CUSTOM_VIEW_PROPERTY( viewindex, index, RNVideoView)
{
    logd( @"setting property*" );
    [_instance viewIndexProperty:json view:view];
}

@end


@interface RN_VideoViewManager()
{
    NSMutableArray* viewList;
    int nextIndex;
}
@end

@implementation RN_VideoViewManager

-(instancetype)init
{
    self = [super init];
    viewList = [NSMutableArray arrayWithCapacity:10];
    nextIndex = 0;
    logi( @"Video manager created*" );
    return self;
}


+(instancetype)Instance
{
    if( _instance == nil )
    {
        _instance = [[RN_VideoViewManager alloc] init];
    }
    
    return _instance;
}


- (UIView *)createView
{
    return [[RNVideoView alloc] init];
}


-(int)removeVideoView:(UIView*)view
{
    for ( int i=0; i < viewList.count; i++ )
    {
        UIView* storedView = viewList[i];
        if( [storedView isEqual:[NSNull null]] )
        {
            continue;
        }
        if( storedView == view )
        {
            [viewList replaceObjectAtIndex:i withObject:[NSNull null] ];
            return i;
        }
    }
    
    return -1;
}


-(int)addVideoView:(UIView*)view type:(NSString*)type
{
    logip( @"VideoView added to list, type:%@", type );
    int i = nextIndex++;
    viewList[i] = view;
    return i;
}


-(void)viewIndexProperty:(id)json view:(RNVideoView*)view
{
    if( json == nil )
    {
        logd( @"nil value, ignored");
        return;
    }
    
    int index = [RCTConvert int:json];
    logip( @"setting remoteViewIndex to %d", index );
    
    if( index < 0 )
    {
        logd( @"negative value, ignored");
        return;
    }
    
    UIView* vv = [viewList objectAtIndex:index];
    
    if( vv == nil )
    {
        loge( @"videoview is null" );
        return;
    }
    
    logi( @"Video view found" );
    [view setVideoView:vv];
}

@end

