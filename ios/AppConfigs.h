//
//  NativeConfigs.h
//  RNLivechatav
//
//  Created by Nuwan Abeysinghe on 25/10/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppConfigs : NSObject
{
}

@property NSString* pauseMusicFile;

@end

extern AppConfigs* g_AppConfigs;

NS_ASSUME_NONNULL_END
