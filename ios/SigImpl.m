//
//  SigImpl.m
//  RNLivechatav
//
//  Created by Nuwan Abeysinghe on 14/9/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#if __has_include("RCTBridgeModule.h")
#import "RCTBridgeModule.h"
#else
#import <React/RCTBridgeModule.h>
#endif

#import "SigImpl.h"

@interface SigImpl()
{
    void(^onSigMessage)(NSString* message);
}

@end

@implementation SigImpl

/*
-(instancetype) initWithOnMsgBlk:(RCTResponseSenderBlock)onMsg
{
    onMessage = onMsg;
    return self;
}
*/

-(instancetype) initWithOnMsgBlk:( void(^)(NSString* message) )onSigMsg
{
    onSigMessage = onSigMsg;
    return self;
}

-(void)sendSigMessage:(NSString*_Nonnull)message
{
    onSigMessage( message );
}

-(void)rtcClose
{
}

@end
