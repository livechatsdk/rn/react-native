//
//  RNVideoView.h
//  RNLivechatav
//
//  Created by Nuwan Abeysinghe on 5/1/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RNVideoView : UIView

-(instancetype)init;
-(void)setVideoView:(UIView *)view;

@end

NS_ASSUME_NONNULL_END
